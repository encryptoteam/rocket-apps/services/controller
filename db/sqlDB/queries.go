package sqlDB

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
)

const queryGetNodeByID = `
	SELECT 	uuid,
			ticker,  
			name, 
			type, 
			blockchain, 
			status, 
			last_update 
	FROM nodes
	WHERE uuid = $1
	LIMIT 1
`

func (c *Connection) GetNodeByID(uuid string) (node model.Node, err error) {
	rows, err := c.db.Query(queryGetNodeByID, uuid)
	if err != nil {
		log.Println("GetNodesAuthData", err)
		return node, err
	}
	defer rows.Close()

	if rows.Next() {
		var lastUpdate string
		err := rows.Scan(
			&node.Uuid,
			&node.Ticker,
			&node.Name,
			&node.Type,
			&node.Blockchain,
			&node.Status,
			&lastUpdate)
		if err != nil {
			log.Println("NodesAuth: parse err", err)
			return node, err
		}
		node.LastUpdate, err = time.Parse(time.RFC3339, lastUpdate)
		if err != nil {
			log.Println("NodesAuth: parse time err", err)
			return node, err
		}
	} else {
		return node, errors.New("node not found")
	}

	return node, err
}

const queryInsertOrUpdateNode = `
	INSERT INTO nodes (
		uuid,
		ticker,  
		name, 
		type, 
		blockchain, 
		status, 
		last_update) 
	VALUES ($1, $2, $3, $4, $5, $6, $7)
	ON CONFLICT (uuid) DO UPDATE 
  	SET ticker 			= excluded.ticker,
		name 			= excluded.name,
		type 			= excluded.type,
		blockchain 		= excluded.blockchain,
  	    status 			= excluded.status,
  	    last_update 	= excluded.last_update;
`

func (c *Connection) UpdateNode(data model.Node) (err error) {
	_, err = c.db.Exec(queryInsertOrUpdateNode,
		data.Uuid,
		data.Ticker,
		data.Name,
		data.Type,
		data.Blockchain,
		data.Status,
		time.Now().Format(time.RFC3339))
	if err != nil {
		log.Println("InsertNodeData", err)
		return err
	}

	return err
}

const queryInsertNodeStatistic = `
	INSERT INTO node_statistics (
		uuid,
		block,
		param,
		value,
		blockchain,
		last_update)
	VALUES ($1, $2, $3, $4, $5, $6)
`

func (c *Connection) InsertNodeStatistic(data model.NodeStatistic) error {
	_, err := c.db.Exec(queryInsertNodeStatistic,
		data.UUID,
		data.Block,
		data.Param,
		data.Value,
		data.Blockchain,
		time.Now().Format(time.RFC3339))
	if err != nil {
		log.Println("InsertNodeServerData", err)
		return err
	}

	return nil
}

const queryGetLastParamData = `
	SELECT 	uuid,
			block,
			param,
			value,
			blockchain,
			last_update
	FROM node_statistics
	WHERE uuid = $1 AND block=$2 AND param=$3
    ORDER BY "last_update" DESC
    LIMIT 1
`

func (c *Connection) GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error) {
	rows, err := c.db.Query(queryGetLastParamData, uuid, block, param)
	if err != nil {
		log.Println("GetLastParam", err)
		return paramData, err
	}
	defer rows.Close()

	if rows.Next() {
		var lastUpdate string
		err = rows.Scan(
			&paramData.UUID,
			&paramData.Block,
			&paramData.Param,
			&paramData.Value,
			&paramData.Blockchain,
			&lastUpdate)
		if err != nil {
			log.Println("GetLastParam: parse err", err)
			return paramData, err
		}
		paramData.LastUpdate, err = time.Parse(time.RFC3339, lastUpdate)
		if err != nil {
			log.Println("GetLastParam: parse time err", err)
			return paramData, err
		}
	} else {
		return paramData, fmt.Errorf("UUID: %s, Block: %s, Param: %s not found", uuid, block, param)
	}

	return paramData, nil
}
