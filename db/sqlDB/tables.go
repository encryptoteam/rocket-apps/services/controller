package sqlDB

import (
	"log"
)

const createNodesTable = `
    CREATE TABLE IF NOT EXISTS nodes (
        uuid            VARCHAR(40)  PRIMARY KEY,
        ticker          VARCHAR(256) NOT NULL DEFAULT '',
        name            VARCHAR(256) NOT NULL DEFAULT '',
        type            VARCHAR(40)  NOT NULL DEFAULT '',
        blockchain      VARCHAR(40)  NOT NULL DEFAULT '',
        status          VARCHAR(40)  NOT NULL DEFAULT '',
        last_update     VARCHAR(70)  NOT NULL
    );
`

const createNodeStatisticsTable = `
    CREATE TABLE IF NOT EXISTS node_statistics (
        uuid            VARCHAR(40) REFERENCES nodes(uuid),
        block           VARCHAR(32) NOT NULL DEFAULT '',
        param           VARCHAR(32) NOT NULL DEFAULT '',
        value           VARCHAR(512) NOT NULL DEFAULT '',
        blockchain      BOOL NOT NULL DEFAULT FALSE,
        last_update     VARCHAR(70) NOT NULL,
        PRIMARY KEY(uuid, block, param, last_update)
    );
`

func (c *Connection) createTables() error {
	var err error
	if _, err = c.db.Exec(createNodesTable); err != nil {
		log.Println("createNodesTable", err)
		return err
	}

	if _, err = c.db.Exec(createNodeStatisticsTable); err != nil {
		log.Println("createNodeStatisticsTable", err)
		return err
	}

	return err
}
