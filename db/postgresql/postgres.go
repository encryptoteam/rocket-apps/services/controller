package postgresql

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
)

func OpenDB(config config.PostgresConfig) (db *sql.DB, err error) {
	log.Println("Opening postgreSQL connection...")

	db, err = sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", config.Host, config.Port, config.User, config.Password, config.DBname, config.SSLmode))
	if err != nil {
		return db, err
	}

	if err = db.Ping(); err != nil {
		return db, err
	}

	return db, nil
}
