package model

import (
	"time"

	commonPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/common"
)

type Node struct {
	commonPB.NodeAuthData
	LastUpdate time.Time
}

type NodeStatistic struct {
	UUID       string
	Block      string
	Param      string
	Value      interface{}
	Blockchain bool
	LastUpdate time.Time
}
