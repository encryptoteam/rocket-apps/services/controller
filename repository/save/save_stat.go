package save

import (
	"log"
	"time"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/db/model"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/informer"
)

type DBNodeStatistic interface {
	GetNodeByID(uuid string) (model.Node, error)
	GetLastParamData(uuid, block, param string) (paramData model.NodeStatistic, err error)

	InsertNodeStatistic(data model.NodeStatistic) error
	UpdateNode(data model.Node) (err error)
}

func AutoSave(server *informer.CardanoInformServer, db DBNodeStatistic, d time.Duration) {
	for {
		time.Sleep(d)
		saveToDb(server, db)
	}
}

func saveToDb(server *informer.CardanoInformServer, db DBNodeStatistic) {
	log.Println("Saving to DB")
	defer log.Println("Saving to DB finished")

	for _, value := range server.NodeStatistics {
		if value.NodeAuthData == nil {
			continue
		}

		if value.Statistic == nil {
			log.Println("stats is nil")
			continue
		}

		dataNodes := model.Node{
			NodeAuthData: *value.NodeAuthData,
		}

		if err := db.UpdateNode(dataNodes); err != nil {
			log.Println(err)
			log.Println("lost data", dataNodes)
			continue
		}

		value.SaveNodeData(db, value.NodeAuthData.Uuid)
	}

	server.NodeStatistics = map[string]informer.NodeStatistics{}
}
