package informer

import (
	"context"
	"log"

	pb "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/common"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/auth"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/repnodes"
)

// CommonInformServer -
type CommonInformServer struct {
	loadedConfig config.Config
	repoNodes    repnodes.RepoNodes

	pb.UnimplementedControllerServer
}

// NewCommonInformServer -
func NewCommonInformServer(jwtManager *auth.JWTManager, loadedConfig config.Config, db DBNodeStatistic) *CommonInformServer {
	return &CommonInformServer{
		loadedConfig: loadedConfig,
		repoNodes:    repnodes.InitController(db),
	}
}

// GetNodeList -
func (server *CommonInformServer) GetNodeList(ctx context.Context, request *pb.GetNodeListRequest) (response *pb.GetNodeListResponse, err error) {
	response = new(pb.GetNodeListResponse)
	for _, n := range server.loadedConfig.Nodes {
		node, err := server.repoNodes.GetNodeByID(n.UUID)
		if err != nil {
			log.Println(err)
			continue
		}

		nodeAuthData := new(pb.NodeAuthData)
		nodeAuthData.Uuid = n.UUID
		nodeAuthData.Ticker = node.NodeAuthData.Ticker
		nodeAuthData.Blockchain = node.NodeAuthData.Blockchain

		nodeAuthData.Type = node.NodeAuthData.Type
		nodeAuthData.Name = node.NodeAuthData.Name

		response.NodeAuthData = append(response.NodeAuthData, nodeAuthData)
	}

	return response, nil
}
