package config

import (
	"log"

	"github.com/bykovme/goconfig"
)

// Config - structure of config file
type Config struct {
	ServerPort  string    `json:"server_port" validate:"required"`
	Nodes       []Node    `json:"nodes"`
	DBConfig    DBConfig  `json:"db"`
	SecretKey   string    `json:"secret_key"`
	TokenTTL    int       `json:"token_ttl"`
	TLS         TLSConfig `json:"tls"`
	usrHomePath string
}

// Node -
type Node struct {
	UUID       string `json:"uuid" validate:"required"`
	Blockchain string `json:"blockchain" validate:"required"`
}

type TLSConfig struct {
	// ConnectionType string   `json:"connection_type"` // "server-side" or "mutual". Default: "server-side"
	CertPath string   `json:"cert_path"`
	IPs      []string `json:"ips"`
}

// ------------------ DBConfig ------------------

// DBConfig ...
type DBConfig struct {
	DBType string `json:"db_type"`

	Postgres PostgresConfig `json:"postgres,omitempty"`
	MySQL    MySQLConfig    `json:"mysql,omitempty"`
	SQLite3  SQLiteConfig   `json:"sqlite3,omitempty"`
}

type PostgresConfig struct {
	// Host - host of postgres server
	Host string `json:"host" validate:"required"`
	// Port - port of postgres server
	Port string `json:"port" validate:"required"`
	// User - user of postgres server
	User string `json:"user" validate:"required"`
	// Password - password of postgres server user
	Password string `json:"password" validate:"required"`
	// DBname - name of database in postgres server
	DBname string `json:"dbname" validate:"required"`
	// SSLmode - ssl mode of postgres server (disable, require, verify-ca, verify-full)
	SSLmode string `json:"sslmode" validate:"required"`
}

type MySQLConfig struct {
	// Host - host of mysql server
	Host string `json:"host" validate:"required"`
	// Port - port of mysql server
	Port string `json:"port" validate:"required"`
	// User - user of mysql server
	User string `json:"user" validate:"required"`
	// Password - password of mysql server user
	Password string `json:"password" validate:"required"`
	// DBname - name of database in mysql server
	DBname string `json:"dbname" validate:"required"`
}

type SQLiteConfig struct {
	// Path - path to sqlite database file
	Path string `json:"path" validate:"required"`
	// DBname - name of database in sqlite file
	DBname string `json:"dbname" validate:"required"`
}

// ------------------ DBConfig ------------------

const cConfigPath = "controller.conf"
const cConfigPathReserve = "etc/ada-rocket/controller.conf"

func LoadConfig() (loadedConfig Config, err error) {
	log.Println("Start loading config...")
	usrHomePath, err := goconfig.GetUserHomePath()
	if err != nil {
		log.Println(err)
		return loadedConfig, err
	}

	loadedConfig.usrHomePath = usrHomePath
	err = goconfig.LoadConfig(cConfigPath, &loadedConfig)
	if err == nil {
		return loadedConfig, nil
	}

	log.Println("Config", usrHomePath+cConfigPath, "not found")
	log.Println("Trying", usrHomePath+cConfigPathReserve, "reserve config path...")
	err = goconfig.LoadConfig(usrHomePath+cConfigPathReserve, &loadedConfig)
	if err != nil {
		log.Println(err)
		err = loadedConfig.CreateConfByUser()
	}

	if loadedConfig.TLS.CertPath == "" {
		loadedConfig.TLS.CertPath = "."
	}

	return loadedConfig, err
}
