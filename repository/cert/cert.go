package cert

import (
	"crypto/tls"
	"crypto/x509/pkix"
	"log"
	"time"

	certTLS "github.com/sergey-shpilevskiy/go-cert/tls"
	certX509 "github.com/sergey-shpilevskiy/go-cert/x509"
	"google.golang.org/grpc/credentials"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/helpers"
)

// TODO: Check if server's certificate and private key are expired

type CertInfo struct {
	certGenerator *certX509.CertGenerator
	tlsConfig     config.TLSConfig
}

// SetupTLS sets up TLS for the server
func SetupTLS(tlsConfig config.TLSConfig) (tlsCredentials credentials.TransportCredentials, err error) {
	// Load server's certificate and private key
	serverCert, err := tls.LoadX509KeyPair(tlsConfig.CertPath+"/server-cert.pem", tlsConfig.CertPath+"/server-key.pem")
	if err != nil {
		log.Println("cannot load server's certificate and private key: ", err)
		log.Println("generating new server's certificate and private key")
		certInfo := CertInfo{}
		return certInfo.GenerateCerts(tlsConfig)
	}

	// Create the credentials and return it
	config := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		ClientAuth:   tls.NoClientCert,
	}

	tlsCredentials = credentials.NewTLS(config)
	return tlsCredentials, err
}

func (c *CertInfo) GenerateCerts(tlsConf config.TLSConfig) (tlsCredentials credentials.TransportCredentials, err error) {
	c.certGenerator = certX509.NewCertGenerator(tlsConf.CertPath, time.Now().Add(time.Hour*24*365*5))
	c.tlsConfig = tlsConf

	if err = c.certGenerator.GenerateCA(controllerCAName); err != nil {
		log.Println(err)
		return tlsCredentials, err
	}

	if tlsCredentials, err = c.GenerateServerCert(); err != nil {
		log.Println(err)
		return tlsCredentials, err
	}

	if err = c.GenerateClientCert(); err != nil {
		log.Println(err)
		return tlsCredentials, err
	}

	return tlsCredentials, err
}

func (c *CertInfo) GenerateServerCert() (tlsCredentials credentials.TransportCredentials, err error) {
	if err = c.certGenerator.GenerateServerCert(serverCertName, helpers.StringToIP(c.tlsConfig.IPs)); err != nil {
		log.Println(err)
		return tlsCredentials, err
	}

	tlsCredentials, err = certTLS.LoadTLSCredentials(c.tlsConfig.CertPath+"/server-cert.pem", c.tlsConfig.CertPath+"/server-key.pem")
	if err != nil {
		log.Println("cannot load TLS credentials: ", err)
		return tlsCredentials, err
	}

	return tlsCredentials, err
}

func (c *CertInfo) GenerateClientCert() (err error) {
	if err = c.certGenerator.GenerateClientCert(clientCertName, helpers.StringToIP(c.tlsConfig.IPs)); err != nil {
		log.Println(err)
		return err
	}

	return err
}

// ------------------------------------------------------------------------------------------

var controllerCAName pkix.Name = pkix.Name{
	CommonName: "Controller CA",
	// Organization: []string{"Cardano Node Operator"},
	// Country:       []string{"US"},
	// Province:      []string{""},
	// Locality:      []string{"San Francisco"},
	// StreetAddress: []string{"Golden Gate Bridge"},
	// PostalCode:    []string{"94016"},
}

var serverCertName pkix.Name = pkix.Name{
	CommonName: "Controller Cert",
	// Organization: []string{"Cardano Node Operator"},
	// Country:       []string{"US"},
	// Province:      []string{""},
	// Locality:      []string{"San Francisco"},
	// StreetAddress: []string{"Golden Gate Bridge"},
	// PostalCode:    []string{"94016"},
}

var clientCertName pkix.Name = pkix.Name{
	CommonName: "Informer Cert",
	// Organization: []string{"Cardano Node Operator"},
	// Country:       []string{"US"},
	// Province:      []string{""},
	// Locality:      []string{"San Francisco"},
	// StreetAddress: []string{"Golden Gate Bridge"},
	// PostalCode:    []string{"94016"},
}

// []net.IP{net.IPv4(127, 0, 0, 1), net.IPv6loopback},
