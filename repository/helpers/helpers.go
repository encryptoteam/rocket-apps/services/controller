package helpers

import (
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
)

func Contains(a []config.Node, UUID string) bool {
	for _, n := range a {
		if n.UUID == UUID {
			return true
		}
	}
	return false
}
