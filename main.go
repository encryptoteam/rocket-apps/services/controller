package main

import (
	"log"
	"net"
	"time"

	authPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/auth"
	cardanoPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/cardano"
	commonPB "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"

	"gitlab.com/encryptoteam/rocket-apps/services/controller/db"
	auth "gitlab.com/encryptoteam/rocket-apps/services/controller/repository/auth"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/cert"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/config"
	informer "gitlab.com/encryptoteam/rocket-apps/services/controller/repository/informer"
	"gitlab.com/encryptoteam/rocket-apps/services/controller/repository/save"
	user "gitlab.com/encryptoteam/rocket-apps/services/controller/repository/user"
)

func main() {
	// grpc.EnableTracing = true

	var secretKey = "secret"

	loadedConfig, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	if loadedConfig.SecretKey != "" {
		secretKey = loadedConfig.SecretKey
	}

	if loadedConfig.TokenTTL == 0 {
		loadedConfig.TokenTTL = 20160 // 2 weeks
	}

	userStore := user.NewInMemoryUserStore()
	if err := seedUsers(userStore); err != nil {
		log.Fatal("cannot seed users: ", err)
	}

	listener, err := net.Listen("tcp", ":"+loadedConfig.ServerPort)
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	// ----------------------------------------------------------------------

	db, err := db.InitDB(loadedConfig.DBConfig)
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	// ----------------------------------------------------------------------

	tlsCredentials, err := cert.SetupTLS(loadedConfig.TLS)
	if err != nil {
		log.Println(err)
		return
	}

	ttl := time.Minute * time.Duration(loadedConfig.TokenTTL)

	jwtManager := auth.NewJWTManager(secretKey, ttl)
	authServer := auth.NewAuthServer(userStore, jwtManager)

	commonServer := informer.NewCommonInformServer(jwtManager, loadedConfig, db)
	cardanoServer := informer.NewCardanoInformServer(jwtManager, loadedConfig, db)

	interceptor := auth.NewAuthInterceptor(jwtManager, accessiblePermissions())

	go save.AutoSave(cardanoServer, db, time.Minute*5)

	grpcServer := grpc.NewServer(
		grpc.Creds(tlsCredentials),
		grpc.UnaryInterceptor(interceptor.Unary()),
	)

	authPB.RegisterAuthServiceServer(grpcServer, authServer)
	commonPB.RegisterControllerServer(grpcServer, commonServer)
	cardanoPB.RegisterCardanoServer(grpcServer, cardanoServer)

	// ----------------------------------------------------------------------

	grpcServer.Serve(listener)
}

// ----------------------------------------------------------------

func createUser(userStore user.UserStore, username, password string, permissions []string) error {
	user, err := user.NewUser(username, password, permissions)
	if err != nil {
		log.Println(err)
		return err
	}
	return userStore.Save(user)
}

func seedUsers(userStore user.UserStore) error {
	if err := createUser(userStore, "admin1", "secret", []string{"basic", "server_technical", "node_technical", "node_financial"}); err != nil {
		log.Println(err)
		return err
	}

	return createUser(userStore, "user1", "secret", []string{"basic"})
}

func accessiblePermissions() map[string][]string {
	const informerServicePath = "/proto.Informer/"

	return map[string][]string{
		informerServicePath + "GetStatistic": {"basic", "server_technical", "node_technical", "node_financial"},
		informerServicePath + "GetNodeList":  {"basic", "server_technical", "node_technical", "node_financial"},
	}
}
